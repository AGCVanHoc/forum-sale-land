<%-- 
    Document   : header
    Created on : Mar 12, 2022, 9:53:54 AM
    Author     : OS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="icon" href="./image/home-land-logo-vector-20319138_1.jpg" type="image/icon type">
        <link href="./img/apple-touch-icon.png" rel="apple-touch-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Real Estate Forum</title>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="./css/formstyle.css"/>
        <link rel="stylesheet" href="./css/style.css" />
    </head>
    <body>
        <a href="#" class="to-top">TOP</a>
        <div class="container-ratio header">
            <div class="overlay"></div>
            <div class="ratio">
                <img src="./image/image-topsearch-bg_01-1024x310.jpg" alt="bg">
            </div>
            <div class="header-box">
                <div class="d-between nav-bar">
                    <div class="logo">
                        <img src="./image/logo-white-2.png" alt="logo">
                    </div>
                    <div class="menu">
                        <ul class="d-center">
                            <li class="menu-item active"><a href="home">Trang chủ</a></li>
                            <li class="menu-item"><a href="#">Giới thiệu</a></li>
                            <li class="menu-item"><a href="#">Dự án</a></li>
                            <li class="menu-item"><a href="#">Tin tức</a></li>
                            <li class="menu-item"><a href="myPost">Tin của bạn</a></li>
                            <c:if test="${sessionScope.ac!=null}">
                                <li class="menu-item"><div class="button-red create" >Gửi bán và cho thuê</div></li>
                                <li class="menu-item"><a class="button-white" href="logout">Đăng xuất</a></li>
                            </c:if>
                            <c:if test="${sessionScope.ac==null}">
                                <li class="menu-item"><div class="button-red create">Gửi bán và cho thuê</div></li>
                                <li class="menu-item"><a class="button-white" href="LoginServlet">Đăng nhập</a></li>
                            </c:if> 

                        </ul>
                    </div>
                </div>
                <div class="banner">
                    <form action="home" method="POST">
                        <label class="search-title" for="search">Tìm kiếm ngôi nhà bạn yêu thích</label>
                        <div class="search-input">
                            <input name="search" type="text" placeholder="Nhập địa điểm, chung cư, tòa nhà...." id="search">
                            <input class="button-red" type="submit" value="Tìm kiếm">
                        </div>
                    </form>
                </div>
                <div class="add-post disable">
                    <div class="add-post-form">
                        <form action="addnew" method="POST">
                            <h1>Add Post</h1>
                            <div>
                                <label for="title">Post title</label>
                                <input name="title" required type="text"/>
                            </div>
                            <div>
                                <label for="price">Price</label>
                                <input name="price" type="number"/>
                            </div>
                            <div>
                                <label for="describe">Describe</label>
                                <textarea name="describe" required type="text"></textarea>
                            </div>
                            <div>
                                <label for="phone">Phone contact</label>
                                <input name="phone" type="number"/>
                            </div>
                            <div>
                                <label for="category">Category</label>
                                <select name="category">
                                    <option value="1">1</option>
                                    <option value="2">2</option> 
                                    <option value="3">3</option> 
                                </select>
                            </div>
                            <input class="submit-post" name="submitpost" type="submit" value="Thêm Post"/>
                        </form>
                    </div> 
                </div>

            </div>
        </div>