<%-- 
    Document   : post
    Created on : Mar 16, 2022, 10:27:16 PM
    Author     : Vinh
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v13.0" nonce="xD4IUnEy"></script>
<script type="text/javascript" src="js/jquery-1.6.2.js"></script>
<div id="fb-root"></div>

  <div class="logo">

  <main class="grid">
    <article>
        <h1 style="margin-top: 20px; margin-left: 10px">
        ${post.post_title}
      </h1>
      <img src="image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="placeholder" class="feature" style="width: 200px; height: 200px; margin-top: 20px">
      <h4>Author: ${post.username.username}</h4>

      <p>${post.describe}</p>

    </article>
      <!--<form name="details" method="post" action="CommentServlet"-->
                <!--onReset="return confirm('Do you really want to clear the form and start again?')"-->
             <!--<p>Comment:<br>
                <textarea id="content" name="comment" rows="3" cols="50" wrap="virtual"></textarea>
            </p>
            <p>
                <input id ="btnComment" type="submit" value="Submit" name="Comment">
                <input type="reset" value="Clear" >
            </p>
            <c:forEach items="${listPost}" begin="0" end="5" var="post">
                 <span>${post.getUsername()}</span>
                 <span>${post.getCmt()}</span>
            </c:forEach>
            <!--</form>-->
            <span id="result1"></span>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#btnComment').click(function () {
                        var comment = $('#content').val();
                        $.ajax({
                            type: 'POST',
                            data: {comment: comment},
                            url: 'CommentServlet',
                            success: function (result) {
                                $('#result1').html(result);
                            }
                        });
                    });
                });
            </script>   
            <div class="fb-comments" data-href="https://batdongsan.com.vn/" data-width="1000" data-numposts="5"></div>

  </main>
 <%@ include file="footer.jsp" %>
