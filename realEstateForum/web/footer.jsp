    <%-- 
    Document   : footer
    Created on : Mar 12, 2022, 9:56:44 AM
    Author     : OS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <section class="footer">
    <div class="footer-container">
      <div class="home-logo">
        <div>
          <img src="./image/logo-white-2.png" alt="logo">
        </div>
        <p class="slogan">
          Là nơi chúng tôi đã lựa chọn rất kỹ càng và cho ra những bất động sản tiềm năng nhất cho tất cả mọi người cùng đầu tư và phát triển.
        </p>
        <p class="address">Đà Nẵng</p>
      </div>
      <div class="help-center">
        <h2>Hỗ trợ</h2>
        <ul>
          <li>Trang chủ</li>
          <li>Giới thiệu</li>
          <li>Bán và cho thuê</li>
          <li>Liên hệ</li>
        </ul>
      </div>
      <div class="help-center">
        <h2>Giới thiệu</h2>
        <ul>
          <li>Dự án</li>
          <li>Mua nhà</li>
          <li>Thuê nhà</li>
          <li>Dự án tại TP HCM</li>
        </ul>
      </div>
      
    </div>
    <div class="copyright">© 2022 - Theme Wordpress bất động sản đẹp</div>
  </section>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="./js/action.js"></script>
</body>
</html>
