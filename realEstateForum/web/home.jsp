<%-- 
    Document   : home
    Created on : Mar 12, 2022, 9:54:47 AM
    Author     : OS
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
  <section class="post-container">
    <div class="title">
      <h2 class="post-title">DỰ ÁN MỚI</h2>
      <p class="sub-title">
        Tổng hợp những mẫu dự án hot nhất trên thị trường bất động sản hiện nay. 
        Liên hệ hotline 0962 682 434 để được hỗ trợ mua nhà nhanh nhất
      </p>
    </div>
    <div class="post-list">
        <c:if test="${posts.size()!=0 && listPost==null}">
             <c:forEach items="${posts}" var="post">
                <div class="post-item">
                    <div class="post-item-image">
                      <a href="listPost?pId=${post.id}&action=show">
                        <div class="container-ratio">
                          <div class="ratio">
                            <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
                          </div>
                        </div>
                        <div class="discount">
                          Thanh toán 30% nhận nhà
                        </div>
                        <div class="saling">
                          Đang mở bán
                        </div>
                      </a>
                    </div>
                    <div class="post-infor">
                      <h4 class="post-name">
                        <a href="#">
                          ${post.post_title}
                        </a>
                      </h4>
                      <p class="post-location">
                        Đào Duy Anh, Gò Vấp, TPHCM
                      </p>
                      <div class="d-between post-price">
                        <div class="price">Giá: <span>${post.price} tr/m2</span></div>
                        <div class="time">SDT: <span>${post.phone_contact}</span></div>
                      </div>
                    </div>
                  </div>
            </c:forEach>
        </c:if>
        <c:if test="${posts.size()==0 && listPost==null}">
            <h1>Không có kết quả phù hợp</h1>
        </c:if>
        <c:forEach items="${listPost}" begin="0" end="5" var="post">
            <div class="post-item">
                <div class="post-item-image">
                  <a href="listPost?pId=${post.id}&action=show">
                    <div class="container-ratio">
                      <div class="ratio">
                        <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
                      </div>
                    </div>
                    <div class="discount">
                      Thanh toán 30% nhận nhà
                    </div>
                    <div class="saling">
                      Đang mở bán
                    </div>
                  </a>
                </div>
                <div class="post-infor">
                  <h4 class="post-name">
                    <a href="#">
                      ${post.post_title}
                    </a>
                  </h4>
                  <p class="post-location">
                    Đào Duy Anh, Gò Vấp, TPHCM
                  </p>
                  <div class="d-between post-price">
                    <div class="price">Giá: <span>${post.price} tr/m2</span></div>
                    <div class="time">SDT: <span>${post.phone_contact}</span></div>
                  </div>
                </div>
              </div>
        </c:forEach>
    </div>
  </section>
  <%@ include file="footer.jsp" %>