<%-- 
    Document   : list-user
    Created on : Mar 16, 2022, 7:40:53 PM
    Author     : PHONG VU
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List User</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="./css/style.css" />
        
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                background: #f7f5f2;
                font-family: 'Roboto', sans-serif;
            }


            .container, .container-lg, .container-md, .container-sm, .container-xl {
                max-width: 100%;
            }

            .table-responsive {
                margin: 30px 0;
            }

            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            }

            .table-title {
                color: #fff;
                background: #40b2cd;
                padding: 16px 25px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }

            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }

            .search-box {
                position: relative;
                float: right;
            }

            .search-box .input-group {
                min-width: 300px;
                position: absolute;
                right: 0;
            }

            .search-box .input-group-addon, .search-box input {
                border-color: #ddd;
                border-radius: 0;
            }

            .search-box input {
                height: 34px;
                padding-right: 35px;
                background: #f4fcfd;
                border: none;
                border-radius: 2px !important;
            }

            .search-box input:focus {
                background: #fff;
            }

            .search-box input::placeholder {
                font-style: italic;
            }

            .search-box .input-group-addon {
                min-width: 35px;
                border: none;
                background: transparent;
                position: absolute;
                right: 0;
                z-index: 9;
                padding: 6px 0;
            }

            .search-box i {
                color: #a0a5b1;
                font-size: 19px;
                position: relative;
                top: 2px;
            }

            table.table {
                table-layout: fixed;
                margin-top: 15px;
            }

            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
            }

            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }

            table.table th:first-child {
                width: 60px;
            }

            table.table th:last-child {
                width: 120px;
            }

            table.table td a {
                color: #a0a5b1;
                display: inline-block;
                margin: 0 5px;
            }

            table.table td a.view {
                color: #03A9F4;
            }

            table.table td a.edit {
                color: #FFC107;
            }

            table.table td a.delete {
                color: #E34724;
            }

            table.table td i {
                font-size: 19px;
            }
        </style>
    </head>
    <body>
        <a href="#" class="to-top">TOP</a>
  <div class="container-ratio header">
    <div class="overlay"></div>
    <div class="ratio">
      <img src="./image/image-topsearch-bg_01-1024x310.jpg" alt="bg">
    </div>
    <div class="header-box">
      <div class="d-between nav-bar">
        <div class="logo">
          <img src="./image/logo-white-2.png" alt="logo">
        </div>
        <div class="menu">
          <ul class="d-center">
            <li class="menu-item active"><a href="listPost">Trang chủ</a></li>
            <li class="menu-item"><a href="#">Giới thiệu</a></li>
            <li class="menu-item"><a href="#">Dự án</a></li>
            <li class="menu-item"><a href="#">Tin tức</a></li>
            <li class="menu-item"><a href="#">Liên hệ</a></li>
            <c:if test="${sessionScope.ac!=null}">
                <li class="menu-item"><a class="button-red" href="addpage">Gửi bán và cho thuê</a></li>
                <li class="menu-item"><a class="button-white" href="logout">Đăng xuất</a></li>
            </c:if>
            <c:if test="${sessionScope.role==null}">
                <li class="menu-item"><a class="button-red" href="login">Gửi bán và cho thuê</a></li>
               <li class="menu-item"><a class="button-white" href="login">Đăng nhập</a></li>
            </c:if> 
            
          </ul>
        </div>
      </div>
      <div class="banner">
        <form>
          <label class="search-title" for="search">Tìm kiếm ngôi nhà bạn yêu thích</label>
          <div class="search-input">
            <input type="text" placeholder="Nhập địa điểm, chung cư, tòa nhà...." id="search">
            <input class="button-red" type="submit" value="Tìm kiếm">
          </div>
        </form>
      </div>
    </div>
  </div>
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-8"><h2>User <b>List</b></h2></div>
                            <div class="col-sm-4">
                                <nav>
                                    <a href="${pageContext.request.contextPath}/listUser?action=add">
                                        <button class="btn btn-success">Add New Employee</button>
                                    </a>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 150px; text-align: center">ID</th>
                                <th style="width: 200px; text-align: center">Name</th>
                                <th style="width: 220px; text-align: center">Phone</th>
                                <th style="width: 200px; text-align: center">Email</th>
                                <th style="width: 150px; text-align: center">Address</th>
                                <th style="width: 220px; text-align: center">Date</th>
                                <th style="width: 150px; text-align: center">User Name</th>
                                <th style="width: 150px; text-align: center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="user" items="${listUser}">
                                <tr>
                                    <td style="text-align: center"><c:out value="${user.id}"/></td>
                                    <td style="text-align: center"><c:out value="${user.name}"/></td>
                                    <td style="text-align: center"><c:out value="${user.phone}"/></td>
                                    <td style="text-align: center"><c:out value="${user.email}"/></td>
                                    <td style="text-align: center"><c:out value="${user.address}"/></td>
                                    <td style="text-align: center"><c:out value="${user.date}"/></td>
                                    <td style="text-align: center"><c:out value="${user.account.username}"/></td>

                                    <td style="display: inline-flex">
                                        <form action="${pageContext.request.contextPath}/listUser"
                                              method="post" style="width: 50px">
                                            <input type="hidden" name="action" value="showEditForm">
                                            <input type="hidden" name="id" value="${user.id}">
                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </form>
                                    
                                        <form action="${pageContext.request.contextPath}/listUser"
                                              method="post" style="width: 50px; margin-left: 30px">
                                            <input type="hidden" name="action" value="delete">
                                            <input type="hidden" name="id" value="${user.id}">
                                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                                    data-target="#confirmDelete${user.id}">Remove
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="confirmDelete${user.id}"
                                                 data-backdrop="static"
                                                 data-keyboard="false" tabindex="-1"
                                                 aria-labelledby="staticBackdropLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="staticBackdropLabel">
                                                                Confirm Delete "${user.name}"?</h5>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            If deleted, you cannot go back.
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Back
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Delete
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>                     

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
      <section class="footer">
    <div class="footer-container">
      <div class="home-logo">
        <div>
          <img src="./image/logo-white-2.png" alt="logo">
        </div>
        <p class="slogan">
          Là nơi chúng tôi đã lựa chọn rất kỹ càng và cho ra những bất động sản tiềm năng nhất cho tất cả mọi người cùng đầu tư và phát triển.
        </p>
        <p class="address">Đà Nẵng</p>
      </div>
      <div class="help-center">
        <h2>Hỗ trợ</h2>
        <ul>
          <li>Trang chủ</li>
          <li>Giới thiệu</li>
          <li>Bán và cho thuê</li>
          <li>Liên hệ</li>
        </ul>
      </div>
      <div class="help-center">
        <h2>Giới thiệu</h2>
        <ul>
          <li>Dự án</li>
          <li>Mua nhà</li>
          <li>Thuê nhà</li>
          <li>Dự án tại TP HCM</li>
        </ul>
      </div>
      
    </div>
    <div class="copyright">© 2022 - Theme Wordpress bất động sản đẹp</div>
  </section>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="./js/action.js"></script>
    </body>
</html>
