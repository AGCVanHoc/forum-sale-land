<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Sign up / Login</title>
  <link rel="stylesheet" href="./style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<!DOCTYPE html>
<html>
<head>
	<title>Slide Navbar</title>
	<link rel="stylesheet" href="style.css">
<link href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap" rel="stylesheet">
</head>
<body>
	<div class="main">  	
		<input type="checkbox" id="chk" aria-hidden="true">

			<div class="signup">
				<form action="SignUpServlet" method="post">
					<label for="chk" aria-hidden="true">Sign up</label>
                                         <p class="text-danger">${mess}</p>
					<input type="text" name="username" placeholder="User name" required="">
					
					<input type="password" name="pass" placeholder="Password" required="">
                                        <input type="repass" name="repass" placeholder="Repeat Password" required="">
					<button>Sign up</button>
				</form>
			</div>

			<div class="login">
				<form action="LoginServlet" method="post">
					<label for="chk" aria-hidden="true">Login</label>
					<input type="text" name="username" placeholder="User Name" required="">
					<input type="password" name="pass" placeholder="Password" required="">
					<input type="submit" value="login"/>
				</form>
			</div>
	</div>
</body>
</html>
<!-- partial -->
  
</body>
</html>
