<%-- 
    Document   : add-user
    Created on : Mar 16, 2022, 8:59:05 PM
    Author     : PHONG VU
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
        <title>>User Management Application</title>
    </head>
    <h2 style="text-align: center">New User</h2>
    <body>
        <div align="center">
    <form  action="${pageContext.request.contextPath}/listUser?action=add" method="post">
        
        <div class="form-row" style="width: 50%; margin-top: 50px">
            
            <div class="form-group">
                <label for="id" style="margin-right: 430px">ID</label>
                <input type="text" name="id" id="id" class="form-control" value="<c:out value='${requestScope.id}'/>"/>
            </div>
              
            <div class="form-group">
                <label for="name" style="margin-right: 430px">Name</label>
                <input type="text" name="name" id="name" class="form-control" value="<c:out value='${requestScope.name}'/>"/>
            </div>
                    
            <div class="form-group">
                <label for="phone" style="margin-right: 430px">Phone</label>
                <input type="text" name="phone" id="phone" class="form-control" value="<c:out value='${requestScope.phone}'/>"/>
            </div>
                    
            <div class="form-group">
                <label for="email" style="padding-right: 430px">Email</label>
                <input type="text" name="email" class="form-control" id="email" value="<c:out value='${requestScope.email}'/>"/>
            </div>
            
            <div class="form-group">
                <label for="address" style="padding-right: 430px">Address</label>
                <input type="text" name="address" class="form-control" id="address" value="<c:out value='${requestScope.address}'/>"/>
            </div>
            
             <div class="form-group">
                <label for="date" style="padding-right: 430px">Date</label>
                <input type="date" id="date" name="date" class="form-control" value="<c:out value='${requestScope.date}'/>"/>
            </div>
                     
            <div class="form-group">
                <label for="username" style="padding-right: 430px">User Name</label>
                <input type="text" name="username" class="form-control" id="username" value="<c:out value='${requestScope.username}'/>"/>
            </div>
           
            <button type="submit" class="btn btn-primary">Save</button>
            
            </div>
    </form>
</div>
    </body>
</html>
