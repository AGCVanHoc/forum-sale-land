/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Post;

/**
 *
 * @author OS
 */
public class ListPost extends HttpServlet {
    PostDAO dao = new PostDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListPost</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListPost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
//        String id = request.getParameter("pId");
        System.out.println("action" + action);
//        System.out.println("pId:" + id);
        if (action == null) {
            action = "";
        }
        
        switch (action) {
            case "create":
                showCreatePost(request, response);
                break;
            case "show":
        {
            try {
                showPost(request, response);
                break;
            } catch (Exception ex) {
                Logger.getLogger(ListPost.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
           
            default:
        {
            try {
                    showListPost(request, response);
            } catch (Exception ex) {
                Logger.getLogger(ListPost.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void showListPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        List<Post> list = dao.getAllPost();
        
        request.setAttribute("listPost", list);
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }
    
    private void showCreatePost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
    }
    
    private  void showPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception{
        String id = request.getParameter("pId");
        
        Post post = dao.GetPostById(id);
        if(id == null) id= "0";
        RequestDispatcher dispatcher;
        request.setAttribute("post", post);
        dispatcher = request.getRequestDispatcher("post.jsp");
        System.out.println("Post: " + post);
        dispatcher.forward(request, response);
    }

}
