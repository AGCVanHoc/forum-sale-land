/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.User;

/**
 *
 * @author PHONG VU
 */
@WebServlet(name = "UsersServlet", urlPatterns = {"/listUser"})
public class UserServlet extends HttpServlet {
    UserDAO dao = new UserDAO();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        if (action == null) {
            action = "";
        }
        
        switch (action) {
            case "add":
                showCreateForm(request, response);
                break;   
                
            default:
                showListUser(request, response);
                break;
        }
    }
    
    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("add-user.jsp").forward(request, response);
    }

    private void showListUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> list = dao.getAllUser();
        
        request.setAttribute("listUser", list);
        request.getRequestDispatcher("list-user.jsp").forward(request, response);
    }



    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        if (action == null) {
            action = "";
        }
        
        switch (action) {
            case "add": 
                createUser(request, response);
                break;
            case "showEditForm":
                showEditForm(request, response);
                break;
            case "edit":
                updateUser(request, response);
                break;
            case "delete":
                deleteUser(request, response);
                break;
            default:
                break;
        }
    }
    
    private void createUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        boolean isValid = true;
        String id = request.getParameter("id");        
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        
        Date date = new Date();
        try {
            String dateString = request.getParameter("date");
            
            if (!dateString.isEmpty()) {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
            } else {
                isValid = false;
                request.setAttribute("errorDate", "Birthday must before today");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
         
           String username = request.getParameter("username");     

        
        if (isValid) {
            dao.addUser(id, name, phone, email, address, date, username);
            
            showListUser(request, response);
        }

        
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isValid = true;
        
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        
        Date date = new Date();
        try {
            String dateString = request.getParameter("date");
            
            if (!dateString.isEmpty()) {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
            } else {
                isValid = false;
                request.setAttribute("errorDate", "Birthday must before today");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        if (isValid) {
        dao.updateUser(id, name, phone, email, address, date);
            
        };
        request.setAttribute("message", "Edit Successfully!");
        showEditForm(request, response);
        
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        
        dao.deleteUser(id);
        request.setAttribute("message", "Delete Successfully!");
        
        showListUser(request, response);
    }

    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        User user = dao.getUserById(id);
        
        request.setAttribute("id", id);
        request.setAttribute("name", user.getName());
        request.setAttribute("phone", user.getPhone());
        request.setAttribute("email", user.getEmail());
        request.setAttribute("address", user.getAddress());
        request.setAttribute("date", user.getDateForInput());
        request.setAttribute("username", user.getAccount().getUsername());
        
        request.getRequestDispatcher("edit-user.jsp").forward(request, response);
    }

}
