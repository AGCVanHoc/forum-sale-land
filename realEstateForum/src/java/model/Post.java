/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author OS
 */
public class Post {
    private String id;
    private String post_title;
    private Date date;
    private String price;
    private String describe;
    private String phone_contact;
    private Account username;
    private String category_id;

    public Post() {
    }

    public Post(String id, String post_title, Date date, Account username, String category_id) {
        this.id = id;
        this.post_title = post_title;
        this.date = date;
        this.username = username;
        this.category_id = category_id;
    }

    public Post(String id, String post_title, Date date, String price, String describe, String phone_contact, Account username, String category_id) {
        this.id = id;
        this.post_title = post_title;
        this.date = date;
        this.price = price;
        this.describe = describe;
        this.phone_contact = phone_contact;
        this.username = username;
        this.category_id = category_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getPhone_contact() {
        return phone_contact;
    }

    public void setPhone_contact(String phone_contact) {
        this.phone_contact = phone_contact;
    }

    public Account getUsername() {
        return username;
    }

    public void setUsername(Account username) {
        this.username = username;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "Post{" + "id=" + id + ", post_title=" + post_title + ", date=" + date + ", price=" + price + ", describe=" + describe + ", author_post=" + ", phone_contact=" + phone_contact + ", username=" + username + ", category_id=" + category_id + '}';
    }
    
}
