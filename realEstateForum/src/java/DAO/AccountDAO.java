/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Account;

/**
 *
 * @author OS
 */
public class AccountDAO {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public Account checkLogin(String username, String password) {
        try {
            String query = "select * from Account where username=? and password=?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while(rs.next()){
                Account account = new Account(rs.getString(1), rs.getString(2), rs.getString(3));
                System.out.println(account);
                return account;
                
            }
        } catch (Exception e) {

        }
        return null;
    }
    
    
   public void signUp(String username, String password){
        String query = "insert into Account values (?,?,0)";
        try{
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
             ps.setString(2, password);
            rs = ps.executeQuery();
             
        }catch(Exception e){
            
        }
       
    }
    public Account checkAccountExist(String username){
        String query = "select * from Account where [username] = ?";
        try{
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while(rs.next()){
                return new Account(rs.getString(1), rs.getString(2), rs.getString(3));
            }
        }catch(Exception e){
            
        }
        return null;
    }
    
}
