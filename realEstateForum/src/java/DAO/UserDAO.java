/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static com.oracle.nio.BufferSecrets.instance;
import context.DBContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Account;
import model.User;
//a
/**
 *
 * @author PHONG VU
 */
public class UserDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;

    // list
    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        String SELECT_ALL_USER = "SELECT * FROM [USER];";

        try {
            
            conn =  getConnection();
            pstmt = conn.prepareStatement(SELECT_ALL_USER);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                list.add(new User (
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("email"),
                        rs.getString("address"),
                        new Date(rs.getDate("date").getTime()),
                        getAccounUsername(rs.getString("username"))
                ));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    // add
    public void addUser(String id, String name, String phone, String email, String address, Date date, String username ) {
        String INSERT_USER = "INSERT INTO [User] (id, name, phone, email, address, date, username) values (?,?,?,?,?,?,?);";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(INSERT_USER);
            
            pstmt.setString(1, id);
            pstmt.setString(2, name);
            pstmt.setString(3, phone);
            pstmt.setString(4, email);
            pstmt.setString(5, address);
            pstmt.setDate(6, new java.sql.Date(date.getTime()));
            pstmt.setString(7, username);
                   

            
            
            pstmt.executeUpdate();
        } catch(SQLException e) {
            System.err.println("Error when inserting!!");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }  
    }
    
    // update 
    public void updateUser(String id, String name, String phone, String email, String address, Date date) {
        String UPDATE_USER = "UPDATE [User] set "
                + "name = ?,"
                + "phone = ?,"
                + "email = ?,"
                + "address = ?,"
                + "date = ? "
                + "where id = ?";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(UPDATE_USER);
            
            pstmt.setString(1, name);
            pstmt.setString(2, phone);
            pstmt.setString(3, email);
            pstmt.setString(4, address);
            pstmt.setDate(5, new java.sql.Date(date.getTime()));
           
            pstmt.setString(6, id);
            
            pstmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
//    
//    // delete
    public void deleteUser(String id) {
        String DELETE_USER = "DELETE FROM [User] WHERE id = ?;";
        
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(DELETE_USER);
            pstmt.setString(1, id);
            pstmt.executeUpdate();
        } catch (Exception throwables) {
            throwables.printStackTrace();
        } 
    }
    
//    // get id
    public User getUserById(String id) {
        String GET_USER_ID = "Select * from [User] Where id = ?";
        
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(GET_USER_ID);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
                return new User(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("email"),
                        rs.getString("address"),
                        new Date(rs.getDate("date").getTime()),
                        getAccounUsername(rs.getString("username"))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } 
 
        return null;
    }

    public  Account getAccounUsername(String username) {
        

        try {
            String SELECT_ACOUNT = "SELECT * FROM [ACCOUNT] WHERE username = ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement pstmt = conn.prepareStatement(SELECT_ACOUNT);
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                return new Account(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3)
                );
            }
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        String url = "jdbc:sqlserver://" + serverName + ":" + portNumber + "\\" + instance + ";databaseName=" + dbName;
        if (instance == null || instance.trim().isEmpty()) {
            url = "jdbc:sqlserver://" + serverName + ":" + portNumber + ";databaseName=" + dbName;
        }
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return DriverManager.getConnection(url, userID, password);
    }
    
    private final String serverName = "DESKTOP-U88RTA2";
    private final String dbName = "RealEstate";
    private final String portNumber = "1433";
    private final String instance = "";//LEAVE THIS ONE EMPTY IF YOUR SQL IS A SINGLE INSTANCE
    private final String userID = "sa";
    private final String password = "Vinhnguyen123";
}