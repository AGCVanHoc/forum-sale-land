/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Post;

/**
 *
 * @author OS
 */
public class PostDAO{
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    public List<Post> getAllPost() throws Exception {
        List<Post> list = new ArrayList<>();
        String SELECT_ALL_POST = "SELECT * FROM Post;";

        try {
            conn =new DBContext().getConnection();
            System.out.println(conn);
            pstmt = conn.prepareStatement(SELECT_ALL_POST);
            rs = pstmt.executeQuery();
            UserDAO userDao = new UserDAO();
            while (rs.next()) {
                list.add(new Post (
                        rs.getString("post_id"),
                        rs.getString("post_title"),
                        new Date(rs.getDate("date").getTime()),
                        rs.getString("price"),
                        rs.getString("describe"),
                        rs.getString("phone_contact"),
                        userDao.getAccounUsername(rs.getString("username")),
                        rs.getString("category_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    public String CreatePost(Post post) throws Exception
    {
        String CREATE_POST = "Insert into Post(post_id, post_title, date, price, describe, phone_contact, username, category_id) output Inserted.post_id "
                + "values(?,?,?,?,?,?,?,?) ";
        String id = null;
        try {
            conn = new DBContext().getConnection();
            pstmt = conn.prepareStatement(CREATE_POST);
            pstmt.setString(1, post.getId());
            pstmt.setString(2, post.getPost_title());
            pstmt.setDate(3, new java.sql.Date(post.getDate().getTime()));
            pstmt.setString(4, post.getPrice());
            pstmt.setString(5, post.getDescribe());
            pstmt.setString(6, post.getPhone_contact());
            pstmt.setString(7, post.getUsername().getUsername());
            pstmt.setString(8, post.getCategory_id());
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                id =rs.getString("post_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }
    
    public String DeletePost(String id) throws Exception{
        String DELETE_POST = "Delete from Post where post_id = ? output deleted.post_id";
        String _id = null;
        try {
            conn = new DBContext().getConnection();
            pstmt = conn.prepareStatement(DELETE_POST);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                _id =rs.getString("post_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return _id;
    }
    
    public String UpdatePost(Post post) throws Exception{
        String DELETE_POST = "Update Post set post_title = ?, price = ?, describe = ?, phone_contact = ?, category_id = ? output inserted.post_id";
        String _id = null;
        try {
            conn = new DBContext().getConnection();
            pstmt = conn.prepareStatement(DELETE_POST);
            pstmt.setString(1, post.getPost_title());
            pstmt.setString(2, post.getPrice());
            pstmt.setString(3, post.getDescribe());
            pstmt.setString(3, post.getPhone_contact());
            pstmt.setString(4, post.getCategory_id());
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                _id =rs.getString("post_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return _id;
    }
    
    public Post GetPostById(String id) throws Exception
    {
        String SELECT_POST = "SELECT * FROM Post WHERE post_id = ?";

        try {
            Connection connection = new DBContext().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_POST);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            UserDAO userDao = new UserDAO();
            if (resultSet.next()) {
                return new Post(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        new Date(resultSet.getDate(3).getTime()),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        userDao.getAccounUsername(resultSet.getString(7)),
                        resultSet.getString(8)
                );
            }
            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    public List<Post> GetPostByUserName(String username){
        List<Post> list = new ArrayList<>();
        String SELECT_POST = "Select * From Post Where username = ?";
        try {
            conn = new DBContext().getConnection();;
            pstmt = conn.prepareStatement(SELECT_POST);
            pstmt.setString(1, username);
            rs = pstmt.executeQuery();
            UserDAO userDao = new UserDAO();
            while(rs.next()){
                list.add(new Post(
                    rs.getString(1),
                    rs.getString(2),
                    new Date(rs.getDate(3).getTime()),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    userDao.getAccounUsername(rs.getString(7)),
                    rs.getString(8)
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
