/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author OS
 */
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Comment;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CommandAPDU;
import model.Account;

public class CommentDAO {

    public static DBContext db = new DBContext();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<Comment> getCommentbyID(int Post_ID) {
        ArrayList<Comment> cmtList = new ArrayList<>();
        try {
            String sql = "select * from Comment where CheckCmt = 1 AND Post_ID = '" + Post_ID + "'";
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Comment cmt = new Comment(rs.getString("id"),
                        rs.getString("post_id"),
                        rs.getString("cmt"),
                        rs.getDate("date"),
                        rs.getString("username"));
                cmtList.add(cmt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cmtList;
    }


    

    public boolean updateCMT(int cmtID) {
        try {
            Statement stm = conn.createStatement();
            String strInsert = "update Comment set CheckCmt = 1 Where CmtID = '" + cmtID + "' ";
            stm.execute(strInsert);
            return true;
        } catch (Exception e) {
            System.out.println("Insert fail: " + e.getMessage());
            return false;
        }
    }

    public boolean addCmt(String account, int idAccount, String content, String date) {
        try {
            Statement stm = conn.createStatement();
            String strInsert = "insert into Comment(post_id,idAccount,content, date) values(" + account + "," + idAccount + "," + content
                    + "," + date + ")";
            stm.execute(strInsert);
            return true;
        } catch (Exception e) {
            System.out.println("Insert fail: " + e.getMessage());
            return false;
        }
    }

    public boolean deleteCMT(int cmtID) {
        try {
            Statement stm = conn.createStatement();
            String strInsert = "delete from Comment Where cmtID = '" + cmtID + "' ";
            stm.execute(strInsert);
            return true;
        } catch (Exception e) {
            System.out.println("Insert fail: " + e.getMessage());
            return false;
        }
    }
    public List<Comment> GetAllCommentByPostID(String postID) throws Exception {
        List<Comment> CommentList = new ArrayList<>();
        String SELECT_ALL_COMMENT = "Select * From Comment where post_id = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(SELECT_ALL_COMMENT);
            ps.setString(1, postID);
            rs = ps.executeQuery();
            while (rs.next()) {                
                CommentList.add(new Comment(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        new Date(rs.getDate(4).getTime()),
                        rs.getString(5)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return CommentList;
    }
}
