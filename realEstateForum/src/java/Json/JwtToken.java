/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Json;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author OS
 */
public class JwtToken {
    private Map<String, Object> jsonMap = new HashMap<String, Object>();
    private String key;
    private Object value;
    public JwtToken(String jsonString) {
        try {
            jsonString = jsonString.substring(1, jsonString.length() -1);
            System.out.println("__________________");
            System.out.println("JsonString: " + jsonString);
            String[] elements = jsonString.split(",");
            for (int i = 0; i < elements.length; i++) {
                System.out.println("Element: " + elements[i]);
                key= elements[i].split(":")[0].substring(1, elements[i].split(":")[0].length() - 1);
                if(elements[i].split(":")[1].charAt(0) != '\"')
                {
                    value= elements[i].split(":")[1].substring(0, elements[i].split(":")[1].length() );
                }
                else
                {
                    value= elements[i].split(":")[1].substring(1, elements[i].split(":")[1].length() - 1);
                }
                jsonMap.put(key, value);
            }
            System.out.println("______________________");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public Map<String, Object> getJsonMap() {
        return jsonMap;
    }

    public void setJsonMap(Map<String, Object> jsonMap) {
        this.jsonMap = jsonMap;
    }
    
    
}
